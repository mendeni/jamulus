#!/bin/bash +xe

cp Jamulus /usr/local/bin

cat<<EOF >/etc/systemd/system/jamulus.service
[Unit]
Description=Jamulus-Central-Server
After=network.target
[Service]
Type=simple
User=jamulus
NoNewPrivileges=true
ProtectSystem=true
ProtectHome=true
Nice=-20
IOSchedulingClass=realtime
IOSchedulingPriority=0
# This line below is what you want to edit according to your preferences
ExecStart=/usr/local/bin/Jamulus --server --nogui \
--log /var/log/jamulus/jamulus.log \
--welcomemessage "<h2>Welcome to Jamulus</h2>" \
--numchannels 10
# end of section you might want to alter
Restart=on-failure
RestartSec=30
StandardOutput=journal
StandardError=inherit
SyslogIdentifier=jamulus
[Install]
WantedBy=multi-user.target
EOF

sudo adduser --system --no-create-home jamulus

sudo mkdir /var/log/jamulus

sudo chown jamulus:nogroup /var/log/jamulus

sudo chmod 644 /etc/systemd/system/jamulus.service

sudo systemctl start jamulus
