#!/bin/bash +xe

VERSION=`date +%Y%m%d.%H%M`

docker build -t jamulus:$VERSION .

IMAGEID=`docker images jamulus:$VERSION | grep -v 'IMAGE ID' | awk '{print $3}'`

docker tag $IMAGEID mendeni/jamulus:latest
