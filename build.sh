#!/bin/bash +xe

apt-get update

apt-get -y install build-essential qt5-qmake qtdeclarative5-dev libjack-jackd2-dev qt5-default git

git clone https://github.com/corrados/jamulus.git jamulus

cd jamulus

qmake "CONFIG+=nosound headless" Jamulus.pro

make clean

make
