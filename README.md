# Usage
1. ./build.sh - Installs build package deps, clones [upstream source](https://github.com/corrados/jamulus), builds Jamulus binary.
1. ./test.sh - Runs simple version check to validate binary will execute.
1. ./image.sh - Creates a container image using Dockerfile.

# Miscellaneous
* ./systemctl.sh - Adds unit to start Jamulus on boot with preconfigured options.
* Built and tested on Ubuntu 18.04, your milage may vary on other OS's.
