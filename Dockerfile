FROM ubuntu:18.04 as jamulus-build
WORKDIR /root
COPY ./ ./
RUN ./build.sh
RUN ./test.sh

FROM ubuntu:18.04 as jamulus
RUN apt-get update
RUN apt-get -y install qt5-default
COPY --from=jamulus-build /root/jamulus/Jamulus /usr/local/bin
EXPOSE 22124
CMD [ "/usr/local/bin/Jamulus", \
      "--server", \
      "--nogui", \
      "--numchannels", "10" ]
